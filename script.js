console.log(`S19 Activity`);

//solution 1-4
let getCube = num => `The cube of ${num} is ${num**3}.`
console.log(getCube(2));

//solution 5
//let address = [`Brgy. San Jose`, `District 5`, `Kalookan City`, `NCR`];
let address = [`258 Washington Ave`, `NW`, `California`, `90011`];

//solution 6
//[brgy, district, city, region] = address;
[street, city, state, zipcode] = address;
//console.log(`${brgy} is located in ${district} of ${city} from ${region}.`);
console.log(`I live at ${street} ${city}, ${state} ${zipcode}`);

//solution 7
let animal = {
	name: `Lolong`,
	breed: `saltwater`,
	type: `crocodile`,
	weight: 1075,
	height: `20 ft 3 in`
}

//solution 8
let {name, breed, type, weight, height} = animal;
console.log(`${name} is a ${breed} ${type}. He weighed ${weight} kgs with a measurement of ${height}.`);

//solution 9
let arrNums = [1, 2, 3, 4, 5];

//solution 10
arrNums.forEach(num=>console.log(num));

//solution 11
let reduceNumber = arrNums.reduce((num1,num2)=>num1+num2);
console.log(reduceNumber);

//solution 12
	class Dog {
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

//solution 13
let myDog = new Dog(`Frankie`, 5, `Miniature Daschund`);
console.log(myDog);

